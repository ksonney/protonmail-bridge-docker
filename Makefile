.PHONY: docker

docker: protonmail-bridge-bin-1.1.4-1-x86_64.pkg.tar.gz initProton.sh gpgparams Dockerfile
	docker build -t t4cc0re/protonmail-bridge .

protonmail-bridge-bin-1.1.4-1-x86_64.pkg.tar.gz: PKGBUILD
	makepkg -f

